package driveitServer;
	
import driveitServer.engine.Drive;
import driveitServer.files.FileParser;
import driveitServer.files.FileWriterClass;
import driveitServer.engine.MovingAverage;
import java.io.IOException;

public class Server {
	    public static final int port=9876;

	    public static void main(String[] args) throws IOException {
                FileParser fp;
	    	if(args.length>0){
                    switch(Integer.parseInt(args[1])){
                        case 1:
                        fp = new FileParser(args[0]);
                        Drive d = new Drive(fp.read());
                        d.learnModel2();
                        break;
                        case 0:
                        fp = new FileParser(args[0]);
	    		MovingAverage window = new MovingAverage(fp.read());
	    		window.ProcessIt();	    		
	    		new FileWriterClass("C:\\accData\\out.txt").printData(window.getData());;
                    }

	    	}else{
		        System.out.println("Starting server...");
		        ClientListener clientListener = new ClientListener();
		        clientListener.run();
	    	}
	    }
}
