package driveitServer.files;

import driveitServer.engine.Maneuver;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;



public class FileWriterClass {
	final String eol = System.getProperty("line.separator");
	BufferedWriter out;
	
	public FileWriterClass(String string) {
		FileWriter fstream;
		try {
			fstream = new FileWriter(string,true);
			out = new BufferedWriter(fstream);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			Close();
		}		
	}
	
	public void printData(ArrayList[]list){
		ArrayList<Float>x=list[0];
		ArrayList<Float>y=list[1];
		ArrayList<Float>z=list[2];
                //ArrayList<Long>timestamps=list[3];
		
		for(int i=0;i<x.size();i++){
			String printStr=String.valueOf(x.get(i))+"\t"+String.valueOf(y.get(i))+"\t"+String.valueOf(z.get(i))+eol;
			try {
				out.write(printStr);
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
				Close();
				break;
			}
		}
	}	
	void Close(){
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public void printManeuvers(ArrayList<Maneuver> maneuver) throws IOException {
        for(int i=0;i<maneuver.size();i++){
            out.write(maneuver.get(i).toString()+"\n");
            out.flush();
        }
    }

    public void printFileHeader() throws IOException {
                out.write("status,time,maxValue,sum,totalSlopeI,totalSlopeD,slopeSumI"
                + ",slopeSumD,maxSlope,starttime,endtime,aggressiveMark\n");
    }
}