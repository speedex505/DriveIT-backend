package driveitServer.files;

import driveitServer.engine.Code;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class FileParser {
	BufferedReader br;
	FileReader fr;
	ArrayList <Float> x;
	ArrayList <Float> y;
	ArrayList <Float> z;
        ArrayList <Long> timestamps;
        ArrayList <Boolean> aggressives;
        ArrayList <Integer> statuses;
	
	ArrayList[] points;
	
	public FileParser(String string) {
		try {
			fr=new FileReader(string);
			br=new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		x=new ArrayList<Float>();
		y=new ArrayList<Float>();
		z=new ArrayList<Float>();
                timestamps= new ArrayList<Long>();
                aggressives= new ArrayList<Boolean>();
                statuses = new ArrayList<Integer>();
	}
	public ArrayList<Float>[] read() throws IOException{
		String strLine;
		while ((strLine = br.readLine()) != null){
			String [] data=strLine.split(",");
			x.add(Float.parseFloat(data[0]));
			y.add(Float.parseFloat(data[1]));
			z.add(Float.parseFloat(data[2]));
                        timestamps.add(Long.parseLong(data[3]));
                        aggressives.add(Boolean.parseBoolean(data[4]));
		} 	
		Close();
		points = new ArrayList[5];
		points[0]=x;
		points[1]=y;
		points[2]=z;
                points[3]=timestamps;
                points[4]=aggressives;
		return points;
	}
	
	private void Close(){
		try {
			fr.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

    public ArrayList[] readMarks() throws IOException {
        String strLine;
        while ((strLine = br.readLine()) != null){
            String [] data=strLine.split(" ");
            timestamps.add(Long.parseLong(data[0]));
            statuses.add(Integer.parseInt(data[1]));
            aggressives.add(Boolean.parseBoolean(data[2]));
        }
        ArrayList[] array = new ArrayList[3];
        array[0]=timestamps;
        array[1]=statuses;
        array[2]=aggressives;
        return array;
    }

    public JSONObject readManeuverStats() throws IOException {
        String strLine;
        int maneuvers=0,accelerations=0,brakings=0,steerings=0;
        int accelerationsAggressive=0,brakingsAggressive=0,steeringsAggressive=0;
        while ((strLine=br.readLine()) !=null){
            String [] data = strLine.split(",");
            maneuvers++;
            int maneuverType = Integer.parseInt(data[0]);
            int aggressive = (Boolean.parseBoolean(data[11])==true)?1:0;
            switch(maneuverType){
                case 0:
                    accelerations++;
                    accelerationsAggressive+=aggressive;
                    break;
                case 1:
                    brakings++;
                    brakingsAggressive+=aggressive;
                    break;
                case 2:
                    steerings++;
                    steeringsAggressive+=aggressive;
                    break;
            }
        }
        JSONObject o = new JSONObject();
            try {
                o.put("action",Code.MANEUVER_RESULT);
                o.put("maneuvers", maneuvers);
                o.put("accelerations", accelerations);
                o.put("brakings", brakings);
                o.put("steerings", steerings);
                o.put("accelAggressive", ((float)accelerations)/accelerationsAggressive);
                o.put("brakingAggressive", ((float)brakings)/brakingsAggressive);
                o.put("steeringAggressive", ((float)steerings)/steeringsAggressive);
            } catch (JSONException ex) {
                return null;
            }        
        return o;
    }
}
