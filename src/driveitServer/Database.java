package driveitServer;

import driveitServer.engine.Code;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	private final String tableUser="user";
    private Connection connection;
    private Statement st;
    private ResultSet rs;
    public Database(){
        try{ 
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver Created");            
        }catch(Exception e){
            System.err.println("Driver error");
            System.err.println(e.toString());
        }

    }    	
    public void open(){
		try {
			connection = (Connection)DriverManager
			.getConnection("jdbc:mysql://127.0.0.1:3306/sabattom","root", "");
	        st = connection.createStatement();
		    System.out.println("statement created");
	        if (connection != null) {
	        	System.out.println("You made it, take control your database now!");
	        } else {
	        	System.err.println("Failed to make connection!");
	        }	 
		} catch (SQLException e) {
			System.err.println("Connection Failed! Check output console");
	                e.printStackTrace();
		}
    }
    public void close(){
    	try {
    		if(connection!=null){
    			connection.close();
    		}
    		if(st!=null){
    			st.close();
    		}
			if(rs!=null){
				rs.close();
			}			
		} catch (SQLException e) {
			System.err.println("ClosingDatabase error");
			e.printStackTrace();
		}
    }

	public synchronized int work(String[] input) {
		int code = Integer.parseInt(input[0]);
		if(code==Code.REGISTER){
			if(commandInsertUser(input[1],input[2])){
				return Code.LISTENING;
			}else{
				return Code.ERROR;
			}
		}
		if(code==Code.LOGIN){
			if(commandLoginUser(input[1],input[2])){
				return Code.LISTENING;
			}else{
				return Code.ERROR;
			}
		}
		return Code.ERROR;		
	}
	
    private boolean commandInsertUser(String username, String password){
        try {
        	st.executeUpdate("INSERT INTO "+tableUser+ 
        						" VALUES (null, '"+username+"','"+password+"')");
        } catch (SQLException ex) {
        	System.out.println(ex.getMessage());
        	return false;
        }        
        return true;
    }
	
	private boolean commandLoginUser(String username, String password) {
        try {
            rs=st.executeQuery("SELECT * FROM "+tableUser+" WHERE account='"+username+"'");
            if(rs.next()){
                String pass=rs.getString("password");
                if(pass.equals(password)){
                	return true;
                }
            }
        } catch (SQLException ex) {
        	System.out.println(ex.getMessage());
            return false;
        }
        return false;
	}    
}