package driveitServer;

import driveitServer.engine.Code;
import driveitServer.files.FileParser;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import org.json.JSONException;
import org.json.JSONObject;

public final class ClientThread implements Runnable{
    Socket socket;
    ObjectOutputStream out;
    ObjectInputStream in;
    private int timeout = 15000;
    
	ClientThread(Socket s) {
	    this.socket = s;	    
	    try {
	    	s.setSoTimeout(timeout);
	        out = new ObjectOutputStream(socket.getOutputStream()); //vytvorit PrintStream
	        in = new ObjectInputStream( socket.getInputStream());
	    }
	    catch(IOException e){
	    	System.err.println("Creating error");
	        e.printStackTrace(System.err);
	        close();
	    }
	}

	void close() {
	    try {
	    	System.out.println("ClientThread: Client disconnected");
	        out.close();
	        in.close();
	        socket.close();
	        //Thread.currentThread().interrupt();
	    } catch(IOException e) {
	    	System.err.println("Closing error");
	    }
	}

	private JSONObject receiveJSON() throws IOException, ClassNotFoundException, JSONException {
		return new JSONObject((String)in.readObject());
	}

	public void sendJSON(JSONObject object) throws IOException {
			out.writeObject(object.toString());
			out.flush();

	}


	@Override
	public void run() {
            try{
            JSONObject send = new JSONObject();
            JSONObject recv;
            send.put("action",Code.LISTENING);
            while(true){
                sendJSON(send);
                System.out.println("ClientThread: JSON("+send.getInt("action")+") sent to ("+socket.getInetAddress()+")");
                recv=receiveJSON();
                System.out.println("ClientThread: JSON("+recv.getInt("action")+") received from("+socket.getInetAddress()+")");
                if(recv.optInt("action",Code.ERROR)==Code.CLOSE||recv.optInt("action",Code.ERROR)==Code.ERROR){
                    break;
                }
                send = processIt(recv);
                if(send.optInt("action",Code.ERROR)==Code.CLOSE||send.optInt("action",Code.ERROR)==Code.ERROR){
                    break;
                }
            }
            }catch(JSONException ex){
                System.err.println(ex.getMessage());
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            } catch (ClassNotFoundException ex) {
                System.err.println(ex.getMessage());
            }
            close();
	}

	private JSONObject processIt(JSONObject input) {
            try{
		int code = input.optInt("action",0);
                JSONObject close = new JSONObject();
                close.put("action",Code.ERROR);
                String username,password;
                switch(code){
                    case Code.REGISTER:case Code.LOGIN:
                        username= input.getString("username");
                        password= input.getString("password");         
                        if(code==Code.REGISTER){
			System.out.println("ClientThread: Registration: "
                                +username+" "+password);
                            return registerUser(username,password);
                        }else{
                            System.out.println("ClientThread: Login: "+username+" "+password);
                            return loginUser(username,password);
                        }
                    case Code.MANEUVER_RESULT_DOWNLOAD:
                        String filename=input.getString("filename");
                        System.out.println("ClientThread: Downloading ManeuverResult: "+filename);
                        return downloadManeuverResult(filename);
                    default:
                    return close;
                }
            }catch(Exception ex){}
            return null;
	}

	private JSONObject loginUser(String username,String password) throws JSONException {
		Database db = new Database();
		db.open();
		int result= db.work(new String[]{""+Code.LOGIN,username,password});
		db.close();
                JSONObject resultJSON= new JSONObject();
                resultJSON.put("action", result);
		return resultJSON;
	}
	private JSONObject registerUser(String username, String password) throws JSONException {
		Database db = new Database();
		db.open();
		int result = db.work(new String[]{""+Code.REGISTER,username,password});
		db.close();
                JSONObject resultJSON= new JSONObject();
                resultJSON.put("action", result);
		return resultJSON;				
	}

    private JSONObject downloadManeuverResult(String filename) throws IOException {
        FileParser fp = new FileParser(filename);
        return fp.readManeuverStats();
    }
}
