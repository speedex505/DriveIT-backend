package driveitServer.engine;

import java.util.ArrayList;

public class Maneuver {
    float totalSlopeI;
    float totalSlopeD;
    float maxSlope;
    float slopeSumI;
    float slopeSumD;
    float sum;
    float maxValue;
    long time;
    boolean aggressiveMark=false;
    
    int start,end;
    long starttime,endtime;
    int status;
    
    final int ACCELERATION=0;
    final int BRAKING=1;
    final int STEERING=2;
    
    final float VALUE_ACCELERATION=14F;
    final float VALUE_BRAKE=12F;
    final float VALUE_STEERING=16F;
    
    
    int recognizeAccelerationManeuver(ArrayList<Float> z, ArrayList<Boolean> aggressiveMarks,int iterator){
        boolean recognized=false;
        start=iterator;
        end=iterator;
        while(true){
            if(iterator>=z.size()){
                return -1;
            }
            if(z.get(iterator)<0.5&& z.get(iterator)>-0.5){
                if(recognized){
                    end=iterator;
                    iterator++;
                    break;                    
                }
                sum=0;
                start=iterator;
                iterator++;
                continue;
            }
            sum+=(z.get(iterator)<0.8&&z.get(iterator)>-0.8)?0F:z.get(iterator);
            if(sum<(VALUE_ACCELERATION*(-1))){
                recognized=true;
                status=ACCELERATION;                
            }
            iterator++;
        }       
        return iterator;
    }
    
    int recognizeBrakingManeuver(ArrayList<Float> z, ArrayList<Boolean> aggressiveMarks,int iterator){
        boolean recognized=false;
        start=iterator;
        end=iterator;
        while(true){
            if(iterator>=z.size()){
                return -1;
            }
            if(z.get(iterator)<0.5&& z.get(iterator)>-0.5){
                if(recognized){
                    end=iterator;
                    iterator++;
                    break;                    
                }
                sum=0;
                start=iterator;
                iterator++;
                continue;
            }
            sum+=(z.get(iterator)<0.8&&z.get(iterator)>-0.8)?0F:z.get(iterator);
            if(sum>VALUE_BRAKE){
                recognized=true;
                status=BRAKING;
            }
            iterator++;
        }       
        return iterator;
    }
    int recognizeSteeringManeuver(ArrayList<Float> y, ArrayList<Boolean> aggressiveMarks,int iterator){
        boolean recognized=false;
        start=iterator;
        end=iterator;
        while(true){
            if(iterator>=y.size()){
                return -1;
            }
            if(y.get(iterator)<0.5&& y.get(iterator)>-0.5||sum<0){
                if(recognized){
                    end=iterator;
                    iterator++;
                    break;                    
                }
                sum=0;
                start=iterator;
                iterator++;
                continue;
            }
            sum+=(y.get(iterator)<0.8&&y.get(iterator)>-0.8)?0F:y.get(iterator);
            if(sum<0)sum*=-1;
            if(sum>VALUE_STEERING){
                recognized=true;
                status=STEERING;
            }
            iterator++;
        }
        return iterator;
    }
    void computeAtributes(ArrayList[] array) {
        float sumI=0,sumD=0;
        int data = (status==ACCELERATION||status==BRAKING)?2:1;
        time=(Long)array[3].get(end)-(Long)array[3].get(start);
        int maxValuePosition=start;
        maxValue=0;
        for(int i=start;i<=end;i++){
            float value =(float)array[data].get(i);
            if(value<0)value*=-1;
            sum+=value;
            if(maxValue<value){
                maxValue=value;
                maxValuePosition=i;
            }
        }
        if(maxValuePosition==start){
            start--;
        }
        if(maxValuePosition==end){
            end--;
        }
        for(int i=start;i<=maxValuePosition;i++){
            float value=(float)array[data].get(i);
            if(value<0)value*=-1;
            sumI+=value;
        }
        
        for(int i=maxValuePosition;i<=end;i++){
            float value=(float)array[data].get(i);
            if(value<0)value*=-1;
            sumD+=value;
        }
        
        Long timeI=(Long)array[3].get(maxValuePosition)-(Long)array[3].get(start);
        Long timeD=(Long)array[3].get(end)-(Long)array[3].get(maxValuePosition);
        float startValue = (float) array[data].get(start);
        float endValue = (float) array[data].get(end);
        if(startValue<0) startValue*=-1;
        if(endValue<0) endValue*=-1;
        totalSlopeI=(maxValue - startValue)/timeI;
        totalSlopeD=(endValue - maxValue)/timeD;
        slopeSumI=totalSlopeI/sumI;
        slopeSumD=totalSlopeD/sumD;
        
        for(int i=start;i<end;i++){
            float slope= (float)array[data].get(i+1)-(float)array[data].get(i-1);
            Long timeS = (Long)array[3].get(i+1)-(long)array[3].get(i-1);
            slope/=timeS;
            slope*=1000;
            if(slope<0)slope*=-1;
            if(slope>maxSlope){
                maxSlope=slope;
            }
        }
        starttime=(long)array[3].get(start);
        endtime  =(long)array[3].get(end);
    }
    
    @Override
    public String toString(){
        String ret=status+","+time+","+maxValue+","+sum+","+totalSlopeI+","+
                totalSlopeD+","+slopeSumI+","+slopeSumD+","+maxSlope+
                ","+starttime+","+endtime+","+aggressiveMark;        
        return ret;
    }
    
    
    
}
