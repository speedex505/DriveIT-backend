package driveitServer.engine;

public class SensorPoint {
    float x;
    float y;
    float z;
    long timeStamp;
    
    public SensorPoint(float x, float y, float z,long timeStamp){
        this.x=x;
        this.y=y;
        this.z=z;
        this.timeStamp=timeStamp;
    }
    
    public float [] getSensorPointAxis(){
        return new float[]{x,y,z};
    }
    public long getTimeStamp(){
        return timeStamp;
    }
    public void setSensorPointAxis(float []axis){
        this.x=axis[0];
        this.y=axis[1];
        this.z=axis[2];
    }
}
