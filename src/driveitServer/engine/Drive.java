package driveitServer.engine;

import driveitServer.files.FileParser;
import driveitServer.files.FileWriterClass;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speedex
 */
public class Drive {
    public final static int X=0;
    public final static int Y=1;
    public final static int Z=2;
    public final static int TIME=3;
    public final static int MARKS=4;
    ArrayList[] array;
    
    ArrayList<Maneuver> accelerations;
    ArrayList<Maneuver> braking;
    ArrayList<Maneuver> steering;
    
    
    public Drive(ArrayList[] array){
        this.array=array;
        accelerations=new ArrayList<Maneuver>();
        braking=new ArrayList<Maneuver>();
        steering=new ArrayList<Maneuver>();
    }
    public void learnModel2() throws IOException {
        loadManeuvers();
        //loadMarks();
        swapData();
        printCSV();
    }
    public float learnModel() throws IOException{
        loadManeuvers();
        swapData();
        printCSV();
        return 0;
    }
    
    public float relativeAggresivity(){
        loadManeuvers();
        return 0;
    }
    
    public void AccelerationModul(){
        
    }
    
    public void loadManeuvers(){
        int iterator =0;
        while(iterator!=-1){
            Maneuver accelerating = new Maneuver();
            iterator=accelerating.recognizeAccelerationManeuver(array[Z], array[MARKS], iterator);
            if(iterator!=-1)
            accelerations.add(accelerating);
        }
        iterator=0;
        while(iterator!=-1){
            Maneuver b = new Maneuver();
            iterator=b.recognizeBrakingManeuver(array[Z], array[MARKS], iterator);
            if(iterator!=-1)
            braking.add(b);
        }
        iterator=0;
        while(iterator!=-1){
            Maneuver s = new Maneuver();
            iterator=s.recognizeSteeringManeuver(array[Y], array[MARKS], iterator);
            if(iterator!=-1)
            steering.add(s);
        }
    }

    private void swapData() throws IOException {
        FileParser fp = new FileParser("out2.csv");
        this.array=fp.read();
        int count=0;
        for(int i=0;i<accelerations.size();i++){
            accelerations.get(i).computeAtributes(array);
            if(accelerations.get(i).aggressiveMark==true)
                count++;
        }
        for(int i=0;i<braking.size();i++){
            braking.get(i).computeAtributes(array);
            if(braking.get(i).aggressiveMark==true)
                count++;
        }
        for(int i=0;i<steering.size();i++){
            steering.get(i).computeAtributes(array);
            if(steering.get(i).aggressiveMark==true)
                count++;
        }
        System.out.println("true: "+count);
        
    }

    private void printCSV() {
        FileWriterClass fw = new FileWriterClass("accel.csv");
        try {
            fw.printFileHeader();
            fw.printManeuvers(accelerations);
            fw.printManeuvers(braking);
            fw.printManeuvers(steering);
        } catch (IOException ex) {
            Logger.getLogger(Drive.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}
