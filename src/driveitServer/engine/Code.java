package driveitServer.engine;

public class Code {
        public static final int CLOSE=500;
        public static final int ERROR=0;
        public static final int LISTENING=1000;
        public static final int LOGIN=1001;
        public static final int REGISTER=1002;
        public static final int MANEUVER_RESULT_DOWNLOAD = 1003;
	public static final int MANEUVER_RESULT = 1004;
}
