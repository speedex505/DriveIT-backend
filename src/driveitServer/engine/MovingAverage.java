package driveitServer.engine;

import java.util.ArrayList;

public class MovingAverage {
	ArrayList<Float> x;
	ArrayList<Float> y;
	ArrayList<Float> z;
        ArrayList<Long> time;
	
	public final static int window = 5;
	
	public MovingAverage(ArrayList<Float>[] points){
		x=points[0];
		y=points[1];
		z=points[2];
		System.out.println(x.size());
	}
	
	public void ProcessIt(){
		for(int i=(window-1)/2;i<x.size()-((window-1)/2);i++){
			float sumX=0,sumY=0,sumZ=0;
			for(int j=i-(window-1)/2;j<i+(window-1)/2;j++){
				sumX+=x.get(j);
				sumY+=y.get(j);
				sumZ+=z.get(j);
			}
			x.set(i, sumX/window);
			y.set(i, sumY/window);
			z.set(i, sumZ/window);
		}		
	}
	
	public ArrayList<Float>[] getData(){
		return new ArrayList[]{x,y,z};
	}
}
